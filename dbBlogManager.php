<?php
include('header.html');
class dbBlogManager{
	private $connectionHandler;



	public function __construct($serverAddress, $username, $password, $databaseName){
		if(!$this->connectionHandler = mysqli_connect($serverAddress, $username, $password)){
			echo("Wystąpił błąd podczas próby połączenia z serwerem MySQL.<br />");
		}
		else{
			echo("Połączenie ustanowione!<br />");
		}

		if(!mysqli_select_db($this->connectionHandler, $databaseName)){
			echo ("Wystąpił błąd podczas wybierania bazy danych.<br />");
		}
		else{
			echo("Baza danych została pomyślnie wybrana.<br />");
		}
	}

	public function __destruct(){
		if(!mysqli_close($this->connectionHandler)){
			echo ("Wystąpił błąd podczas zamykania połączenia z serwerem MySQL.<br />");
		}
		else{
			echo ("Zakończono połączenie z bazą danych.<br />");
		}

	}

	public function addPostToDataBase($topic, $text){
		echo("TEST");
		$currentDate = date('Y-m-d H:i:s');
		$insertQuery = "INSERT INTO wpisy(topic, content, posting_date) VALUES(\"".$topic."\", \"".$text."\", \"".$currentDate."\")";

		if(!$result = mysqli_query($this->connectionHandler, $insertQuery)){
			echo ("Błąd podczas wysyłania zapytania.<br />");
		}
		else{
			echo ("QUERY OK<br />");
		}
		
	}
	public function deletePostFromDatabase($postID){
		$deleteQuery = "DELETE FROM wpisy WHERE id = \"".$postID."\"";

		if(!$result = mysqli_query($this->connectionHandler, $deleteQuery)){
			echo("Usuwanie zakończone niepowodzeniem.<br />");
		}
		else{
			echo("QUERY OK<br />");
		}
	}

	public function getNextPost($postID){
		$getPostQuery = "SELECT * FROM wpisy WHERE id > \"".$postID."\" order by id ASC limit 1";

		if(!$result = mysqli_query($this->connectionHandler, $getPostQuery)){
			echo("Nie mozna wyciagnac wpisu<br />");
		}
		else{
			echo("QUERY OK<br />");
			$post;
			while($row = mysqli_fetch_array($result)){
				$post = $row;
			}
		}
		return $post;
	}
	public function getPreviousPost($postID){
		$getPostQuery = "SELECT * FROM wpisy WHERE id < \"".$postID."\" order by id DESC limit 1";

		if(!$result = mysqli_query($this->connectionHandler, $getPostQuery)){
			echo("Nie mozna wyciagnac wpisu<br />");
		}
		else{
			echo("QUERY OK<br />");
			$post;
			while($row = mysqli_fetch_array($result)){
				$post = $row;
			}
		}
		return $post;	
	}
	public function getFirstPost(){
		$getFirstPostQuery = "SELECT * FROM wpisy order by id ASC limit 1";
		
		if(!$result = mysqli_query($this->connectionHandler, $getFirstPostQuery)){
			echo ("Nie można wyciągnąć wpisu<br />");
		}
		else{
			echo ("QUERY OK<br />");
			$post;
			while($row = mysqli_fetch_array($result)){
				$post = $row;
			}
		}
		return $post;
	}


}
include('footer.html');
?>
